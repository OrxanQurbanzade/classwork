import React, {Component} from 'react';
import {Link,Route} from "react-router-dom";

const User = () => <h1>Let gogi go home😫</h1>;
const Login = ()=><h1>Gogi can't access this site🙄</h1>
const Register = () =>  <h1>Allow Gogi to register😁</h1>

class App extends Component {
  render() {
    return (
        <>
          <h2>Simple react route application</h2>
          <p>Gogi approves this project</p>

            <Link to={'/user'}>Home<br/></Link>
            <Link to={'/login'}>Login<br/></Link>
            <Link to={'/register'}>Register</Link>

            <Route path={'/user'} component={User}/>
            <Route path={'/login'} component={Login}/>
            <Route path={'/register'} component={Register}/>

        </>
    );
  }
}

export default App;
